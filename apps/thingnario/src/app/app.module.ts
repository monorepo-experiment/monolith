import { FeatureEmsModule } from '@monolith/feature/ems';
import { FeatureVppModule } from '@monolith/feature/vpp';
import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [FeatureEmsModule, FeatureVppModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
