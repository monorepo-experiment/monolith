import { Module } from '@nestjs/common';
import { SharedUserService } from './shared-user.service';

@Module({
  controllers: [],
  providers: [SharedUserService],
  exports: [SharedUserService],
})
export class SharedUserModule {}
