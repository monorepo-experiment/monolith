import { SharedUserService } from '@monolith/shared/user';
import { Controller, Get, Query } from '@nestjs/common';

@Controller('vpp')
export class FeatureVppController {
  constructor(private userService: SharedUserService) {}

  @Get('hello')
  hello(@Query('token') token: string): string {
    if (!this.userService.validate(token)) {
      return 'invalid';
    }
    return 'world vpp';
  }
}
