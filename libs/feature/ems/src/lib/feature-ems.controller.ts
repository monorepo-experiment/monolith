import { SharedUserService } from '@monolith/shared/user';
import { Controller, Get, Query } from '@nestjs/common';

@Controller('ems')
export class FeatureEmsController {
  constructor(private userService: SharedUserService) {}

  @Get('hello')
  hello(@Query('token') token: string): string {
    if (!this.userService.validate(token)) {
      return 'invalid';
    }
    return 'world ems';
  }
}
