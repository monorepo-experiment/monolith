import { SharedUserModule } from '@monolith/shared/user';
import { Module } from '@nestjs/common';
import { FeatureEmsController } from './feature-ems.controller';

@Module({
  imports: [SharedUserModule],
  controllers: [FeatureEmsController],
  providers: [],
  exports: [],
})
export class FeatureEmsModule {}
